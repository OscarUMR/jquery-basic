$('.jquery__append-text').click(function() {
  $('.jquery__append p').append('<b>texto añadido</b>');
});
$('.jquery__append-list').click(function() {
  $('.jquery__append ol').append('<li>texto añadido</li>');
});

//*prepend*//
$('.jquery__prepend-text').click(function() {
    $('.jquery__prepend p').prepend('<b>Proponer</b>');
  });
$('.jquery__prepend-list').click(function() {
    $('.jquery__prepend ol').prepend('<li>prepend</li>');
});

//* after-before*//
$('.jquery__after').click(function() {
    $('.jquery__after-before p').after('<b>despues</b>');
  });
$('.jquery__before').click(function() {
    $('.jquery__after-before p').before('<span><b>antes</b></span>');
});

//* remove-empty*//
$('.jquery__remove-button').click(function() {
    $('.jquery__remove-div').remove();
  });
$('.jquery__empty-button').click(function() {
    $('.jquery__remove-div').empty();
});

$('.jquery__add-class').click(function() {
    $('.jquery__add-class button').addClass('success');
});
//remove-class//
$('.jquery__remove-class button').click(function() {
  $('.jquery__remove-class .error').removeClass('error');
});
//toggleclass//
$('.jquery__toggle-class').click(function() {
  $('.jquery__toggle-class button').toggleClass('success');
});
//css//
$('.jquery__css button').click(function() {
  $('.jquery__css button').css('background-color','blue');
});
//attr//
$('.jquery__attr button').click(function() {
  $('.jquery__attr a').attr('href','https://google.com');
});
//removeattr//
$('.jquery__attr-remove button').click(function() {
  $('.jquery__attr-remove a').removeAttr('href','https://google.com');
});
//slidetoggle//
$('.jquery__slide-toggle button').click(function() {
  $('.jquery__slide-toggle p').slideToggle();
});
//fadetoggle//
$('.jquery__fade-toggle button').click(function() {
  $('.jquery__fade-toggle p').fadeToggle();
});
//toggle//
$('.jquery__toggle button').click(function() {
  $('.jquery__toggle p').toggle();
});
//show//
$('.jquery__show button').click(function() {
  $('.jquery__show p').show();
});
//hide//
$('.jquery__hide button').click(function() {
  $('.jquery__hide p').hide();
});
//first//
$('.jquery__first button').click(function() {
  $('.jquery__first p').first().addClass('success');
});
//last//
$('.jquery__last button').click(function() {
  $('.jquery__last p').last().addClass('success');
});
